from django.db import models
from account.models import Doctor
from location.models import City


class MedicalCenter(models.Model):
    name = models.CharField(max_length=100)
    location = models.ForeignKey(City, on_delete=models.SET_NULL, null=True)

    class Meta:
        unique_together = (
            "name",
            "location",
        )

    def __str__(self):
        return f"{self.location} - {self.name}"


class MedicalCenterDoctor(models.Model):
    medical_center = models.ForeignKey(
        MedicalCenter,
        on_delete=models.SET_NULL,
        null=True,
        related_name="medical_center",
    )
    doctor = models.ForeignKey(
        Doctor, on_delete=models.SET_NULL, null=True, related_name="medical_centers"
    )

    class Meta:
        unique_together = (
            "medical_center",
            "doctor",
        )

    def __str__(self):
        return f"{self.medical_center.name} - {self.doctor}"
