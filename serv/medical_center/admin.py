from django.contrib import admin

from .models import MedicalCenter, MedicalCenterDoctor


admin.site.register(MedicalCenter)
admin.site.register(MedicalCenterDoctor)