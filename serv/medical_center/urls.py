from django.urls import path
from . import views

urlpatterns = [
    path("<int:pk>/", views.MedicalCenterList.as_view()),
    path("", views.MedicalCenterDoctor.as_view()),
]