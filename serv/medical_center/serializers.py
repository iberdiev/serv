from . import models
from rest_framework import serializers


class MedicalCenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MedicalCenter
        fields = ("name", "pk")
