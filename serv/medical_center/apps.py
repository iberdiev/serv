from django.apps import AppConfig


class MedicalCenterConfig(AppConfig):
    name = 'medical_center'
