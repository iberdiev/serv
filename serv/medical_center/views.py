from rest_auth.views import APIView
from rest_framework.response import Response
from . import models, serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes
from rest_framework import status


class MedicalCenterList(APIView):
    def get(self, request, pk):
        centers = models.MedicalCenter.objects.filter(location=pk)
        data = serializers.MedicalCenterSerializer(centers, many=True).data
        return Response(data)


@permission_classes((IsAuthenticated,))
class MedicalCenterDoctor(APIView):
    def post(self, request):
        if "pk" not in request.data:
            message = "Please provide pk of the medical center you want to assign"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
        if models.MedicalCenter.objects.filter(pk=request.data["pk"]).exists():
            if models.MedicalCenterDoctor.objects.filter(
                doctor=request.user.doctor, medical_center=request.data["pk"]
            ).exists():
                return Response("This medical center is already assigned")
            models.MedicalCenterDoctor.objects.create(
                medical_center=models.MedicalCenter.objects.get(id=request.data["pk"]),
                doctor=request.user.doctor,
            )
            return Response("Assigned medical center successfully.")
        else:
            message = "Medical center with specifed pk does not exist"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        if "pk" not in request.query_params:
            return Response("pk was not provided", status=status.HTTP_400_BAD_REQUEST)
        if models.MedicalCenterDoctor.objects.filter(
            doctor=request.user.doctor, medical_center=request.query_params["pk"]
        ).exists():
            models.MedicalCenterDoctor.objects.filter(
                doctor=request.user.doctor, medical_center=request.query_params["pk"]
            ).delete()
            return Response("Unassigned medical center successfully.")
        else:
            message = "Cannot remove not assigned medical center"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
