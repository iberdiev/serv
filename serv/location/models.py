from django.db import models


class Country(models.Model):
    country_name = models.CharField(max_length=50, unique=True)
    country_ISO = models.CharField(max_length=10)

    def __str__(self):
        return self.country_name


class City(models.Model):
    city_name = models.CharField(max_length=50)
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True)

    class Meta:
        unique_together = (
            "city_name",
            "country",
        )

    def __str__(self):
        return f"{self.city_name} - {self.country}"
