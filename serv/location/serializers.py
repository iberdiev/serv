from . import models
from rest_framework import serializers


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Country
        fields = ("country_name", "country_ISO", "pk")


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.City
        fields = (
            "pk",
            "city_name",
        )
