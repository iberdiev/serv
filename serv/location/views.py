from . import serializers, models
from rest_framework import generics
from rest_framework.response import Response


class CountryList(generics.ListAPIView):
    queryset = models.Country.objects.all()
    serializer_class = serializers.CountrySerializer



class CityList(generics.ListAPIView):
    serializer_class = serializers.CitySerializer
    def get_queryset(self):
        country = self.request.query_params.get('country')
        cities = models.City.objects.filter(country__country_ISO=country) if country else models.City.objects.all()
        return cities

