from django.urls import path
from . import views

urlpatterns = [
    path("category/", views.CategoryList.as_view()),
    path("category/<int:pk>/", views.IllnessList.as_view()),
    path("doctor-illness/", views.AssignRemoveIllness.as_view()),
]