from django.contrib import admin

# Register your models here.
from .models import Illness, Category


admin.site.register(Illness)
admin.site.register(Category)