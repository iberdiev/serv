from . import serializers, models
from rest_framework import generics
from rest_framework.response import Response
from rest_auth.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes
from rest_framework import status
from account.models import DoctorSpecialization


class CategoryList(generics.ListAPIView):
    queryset = models.Category.objects.all()
    serializer_class = serializers.CategorySerializer


class IllnessList(APIView):
    def get(self, request, pk):
        illnesses = models.Illness.objects.filter(category=pk)
        data = serializers.IllnessSerializer(illnesses, many=True).data
        return Response(data)


@permission_classes((IsAuthenticated,))
class AssignRemoveIllness(APIView):
    def post(self, request):
        if "pk" not in request.data:
            message = "Please provide id of the illness you want to assign"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
        if models.Illness.objects.filter(id=request.data["pk"]).exists():
            if DoctorSpecialization.objects.filter(
                doctor=request.user.doctor, illness__pk=request.data["pk"]
            ).exists():
                return Response("This illness is already assigned")
            DoctorSpecialization.objects.create(
                illness=models.Illness.objects.get(id=request.data["pk"]),
                doctor=request.user.doctor,
            )
            return Response("Assigned illness successfully.")
        else:
            message = "Illness with specifed pk does not exist"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        if "pk" not in request.query_params:
            return Response("pk was not provided", status=status.HTTP_400_BAD_REQUEST)
        if DoctorSpecialization.objects.filter(
            doctor=request.user.doctor, illness__pk=request.query_params["pk"]
        ).exists():
            DoctorSpecialization.objects.filter(
                doctor=request.user.doctor, illness__pk=request.query_params["pk"]
            ).delete()
            return Response("Unassigned illness successfully.")
        else:
            message = "Cannot remove not assigned illness"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
