from . import models
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = ("name", "pk")


class IllnessSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Illness
        fields = (
            "name",
            "pk",
        )
