from django.apps import AppConfig


class IllnessConfig(AppConfig):
    name = 'illness'
