from django.db import models
from django.contrib.auth.models import AbstractUser
from location.models import City
from illness.models import Illness


class CustomUser(AbstractUser):
    email = models.EmailField(max_length=50, unique=True)
    phone_number = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(blank=True, null=True)
    location = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, blank=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    TYPE_CHOICES = (
        (1, "Doctor"),
        (2, "Patient"),
    )
    user_role = models.PositiveSmallIntegerField(
        null=True, blank=True, choices=TYPE_CHOICES
    )
    created_date = models.DateTimeField("date_created", auto_now_add=True, null=True)
    description = models.TextField(max_length=1000, default="")
    profile_image = models.ImageField(upload_to="profile/", null=True, blank=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Patient(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.user)


class Profession(models.Model):
    title = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.title


class Doctor(models.Model):
    user = models.OneToOneField(
        CustomUser, on_delete=models.CASCADE, null=True, related_name="doctor"
    )
    profession = models.ForeignKey(Profession, on_delete=models.SET_NULL, null=True)

    class Meta:
        unique_together = (
            "user",
            "profession",
        )

    def __str__(self):
        return f"{self.user} - {self.profession}"


class DoctorSpecialization(models.Model):
    doctor = models.ForeignKey(
        Doctor, on_delete=models.SET_NULL, null=True, related_name="illnesses"
    )
    illness = models.ForeignKey(Illness, on_delete=models.SET_NULL, null=True)

    class Meta:
        unique_together = (
            "doctor",
            "illness",
        )

    def __str__(self):
        return str(self.doctor) + " - " + str(self.illness)


class AppointmentRequest(models.Model):
    doctor = models.ForeignKey(
        CustomUser, on_delete=models.SET_NULL, null=True, related_name="requests"
    )
    description = models.TextField(max_length=5000, default="")


class Example(models.Model):
    example = models.CharField(max_length=50)