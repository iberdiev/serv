from django.test import TestCase
from .models import CustomUser, Profession, Doctor, Patient
from location.models import Country, City
from django.core.mail import send_mail
from serv.settings import EMAIL_HOST_USER
from django.test.utils import override_settings
from rest_framework.test import APIRequestFactory, force_authenticate
from location.views import CityList
from rest_framework.test import APIClient
import json

client = APIClient()


@override_settings(EMAIL_BACKEND="django.core.mail.backends.smtp.EmailBackend")
class CustomUserTestCase(TestCase):
    def setUp(self):
        country = Country.objects.create(
            country_name="Sample Country", country_ISO="sample_ISO"
        )
        city = City.objects.create(city_name="Sample City", country=country)
        users = [
            {
                "email": "doctor@doctor.doctor",
                "first_name": "Doctor Name",
                "last_name": "Doctor Last Name",
                "user_role": 1,
                "password": "doctor",
                "username": "doctor@doctor.doctor",
            },
            {
                "email": "patient@patient.patient",
                "first_name": "Patient Name",
                "last_name": "Patient Last Name",
                "user_role": 2,
                "password": "patient",
                "username": "patient@patient.patient",
            },
        ]
        profession = Profession.objects.create(title="Therapist")
        for u in users:
            user = CustomUser.objects.create(
                username=u["username"],
                email=u["email"],
                first_name=u["first_name"],
                last_name=u["last_name"],
                user_role=u["user_role"],
                location=city,
            )
            user.set_password(u["password"])
            user.save()
            if u["user_role"] == 1:
                Doctor.objects.create(user=user, profession=profession)
            elif u["user_role"] == 2:
                Patient.objects.create(user=user)

    def test_user_details(self):
        try:
            doctor = Doctor.objects.all()[0]
            patient = Patient.objects.all()[0]
            self.assertEqual(
                doctor.profession, Profession.objects.get(title="Therapist")
            )
            self.assertEqual(doctor.user.location, City.objects.get(pk=1))
            self.assertEqual(patient.user.location, City.objects.get(pk=100))

        except Exception as e:
            send_mail(
                "Test failure",
                str(e),
                EMAIL_HOST_USER,
                [
                    "iskender.berdiev@gmail.com",
                ],
                fail_silently=False,
            )
            print("zxcv", e)

    def test_location_api(self):
        print("zxcv")
        try:
            response = client.get("/api/v1/location/city/")
            self.assertEqual(
                json.loads(response.content), [{"pk": 1, "city_name": "Sample City"}]
            )
        except Exception as e:
            send_mail(
                "Test failure",
                str(e),
                EMAIL_HOST_USER,
                [
                    "iskender.berdiev@gmail.com",
                ],
                fail_silently=False,
            )
            print("asdf", e)