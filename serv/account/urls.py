from django.urls import path
from . import views
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path("registration/", views.UserCreateAPIView.as_view()),
    path("api-token-auth/", views.CustomLoginView.as_view()),
    path("profile/", views.ProfileRetrieveUpdateView.as_view()),
    path("profile/change_password/", views.ChangePasswordView.as_view()),
    path("profile/update/", views.UpdateProfileView.as_view()),
    path("doctors/", views.DoctorsListView.as_view()),
    path("doctors/profession/", views.DoctorProfessionView.as_view()),
    path("doctors/search/", views.DoctorsSearchView.as_view()),
    path("doctors/appointment/", views.DoctorAppointment.as_view()),
    path("contact/", views.Contact.as_view()),
]
