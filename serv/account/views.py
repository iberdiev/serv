from rest_framework import generics, status, permissions
from rest_framework.permissions import IsAuthenticated
from . import models, serializers, forms
from location.models import City
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_auth.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from serv.settings import EMAIL_HOST_USER
from django.core.mail import send_mail


class CustomLoginView(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        if not request.data["username"] or not request.data["password"]:
            message = "Please provide username and password"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
        user = models.CustomUser.objects.filter(username=request.data["username"])
        if not len(user):
            message = "Not found user with provided username"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
        user = user[0]
        if user.check_password(request.data["password"]):
            token, created = Token.objects.get_or_create(user=user)
            return Response(
                {
                    "token": token.key,
                    "user_role": user.user_role,
                }
            )
        else:
            return Response("Wrong password.")


class UserCreateAPIView(generics.CreateAPIView):
    queryset = models.CustomUser.objects.all()
    serializer_class = serializers.UserSerializer

    def perform_create(self, serializer):
        user_role = int(self.request.data["user_role"])
        username = self.request.data["email"]
        password = self.request.data["password"]
        location = int(self.request.data["location"])
        location = City.objects.get(pk=location)
        serializer.save(username=username, location=location)
        user = models.CustomUser.objects.get(email=username)
        user.set_password(password)
        user.save()

        if user_role == 1:
            models.Doctor.objects.create(user=user)
        elif user_role == 2:
            models.Patient.objects.create(user=user)


@permission_classes((IsAuthenticated,))
class ProfileRetrieveUpdateView(APIView):
    def get(self, request):
        if request.user.user_role == 1:
            data = serializers.DoctorDetailSerializer(request.user).data
        elif request.user.user_role == 2:
            data = serializers.PatientDetailSerializer(request.user).data
        return Response(data)


class ChangePasswordView(generics.UpdateAPIView):
    serializer_class = serializers.ChangePasswordSerializer
    model = models.CustomUser
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response(
                    {"old_password": ["Wrong password."]},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                "status": "success",
                "code": status.HTTP_200_OK,
                "message": "Password updated successfully",
                "data": [],
            }
            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((IsAuthenticated,))
class DoctorsListView(APIView):
    def get(self, request):
        if self.request.query_params.get("location"):
            doctors = models.Doctor.objects.filter(
                user__location=int(self.request.query_params.get("location"))
            )
        elif self.request.query_params.get("country_ISO"):
            doctors = models.Doctor.objects.filter(
                user__location__country__country_ISO=self.request.query_params.get(
                    "country_ISO"
                )
            )
        else:
            doctors = models.Doctor.objects.all()
        doctors = [doctor.user for doctor in doctors]
        data = serializers.DoctorDetailSerializer(doctors, many=True).data
        return Response(data)


class UpdateProfileView(generics.UpdateAPIView):
    serializer_class = serializers.UpdateProfileSerializer
    model = models.CustomUser
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            if "old_password" in request.data:
                if "new_password" not in request.data:
                    return Response(
                        "You provided old_password. Please provided new_password as well to update the password.",
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                if not self.object.check_password(serializer.data.get("old_password")):
                    return Response(
                        {"old_password": ["Wrong password."]},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                self.object.set_password(serializer.data.get("new_password"))
            if "first_name" in request.data:
                self.object.first_name = request.data["first_name"]
            if "last_name" in request.data:
                self.object.last_name = request.data["last_name"]
            if "description" in request.data:
                self.object.description = request.data["description"]
            if "phone_number" in request.data:
                self.object.phone_number = request.data["phone_number"]
            if "location" in request.data:
                self.object.location = City.objects.get(id=request.data["location"])
            if "birth_date" in request.data:
                self.object.birth_date = request.data["birth_date"]
            if "profile_image" in request.data:
                self.object.profile_image = request.data["profile_image"]

            self.object.save()
            response = {
                "status": "success",
                "code": status.HTTP_200_OK,
                "message": "Updated successfully",
                "data": [],
            }
            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((IsAuthenticated,))
class DoctorProfessionView(APIView):
    def get(self, request):
        data = serializers.ProfessionSerializer(
            models.Profession.objects.all(), many=True
        ).data
        return Response(data)

    def post(self, request):
        if "pk" not in request.data:
            message = "Please provide pk of the profession to assign"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
        if models.Profession.objects.filter(id=request.data["pk"]).exists():
            if models.Doctor.objects.filter(
                user=request.user, profession=request.data["pk"]
            ).exists():
                return Response("This profession is already assigned")
            doctor = request.user.doctor
            doctor.profession = models.Profession.objects.get(pk=request.data["pk"])
            doctor.save()
            return Response("Assigned profession successfully.")
        else:
            message = "Profession with specifed pk does not exist"
            return Response(message, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((IsAuthenticated,))
class DoctorsSearchView(APIView):
    def get(self, request):
        params = request.query_params
        doctors = models.Doctor.objects.all()
        if "location" in params and params["location"]:
            doctors = doctors.filter(user__location=params["location"])
        if "country" in params and params["country"]:
            doctors = doctors.filter(user__location__country=params["country"])
        if "hospital" in params and params["hospital"]:
            doctors = doctors.filter(user__doctor__medical_centers=params["hospital"])
        if "profession" in params and params["profession"]:
            doctors = doctors.filter(user__doctor__profession=params["profession"])
        if "category" in params and params["category"]:
            doctors = doctors.filter(illnesses__illness__category=params["category"])
        if "illness" in params and params["illness"]:
            doctors = doctors.filter(illnesses__illness=params["illness"])
        doctors = [doctor.user for doctor in doctors]
        data = serializers.DoctorDetailSerializer(doctors, many=True).data
        return Response(data)


@permission_classes((IsAuthenticated,))
class DoctorAppointment(APIView):
    def post(self, request):
        if request.user.user_role == 1:
            return Response("You are doctor, you cannot send appointment request.")
        elif request.user.user_role == 2:
            doctor = models.CustomUser.objects.get(pk=request.data["pk"])
            recepient = doctor.username
            sub = forms.Subscribe(request.POST)
            subject = "New Appointment Request"
            message = f'Hello doctor {doctor}\n\nPatient {request.user} requested an appointment for {request.data["month"]} {request.data["day"]}, {request.data["year"]}, {request.data["hour"]}:{request.data["minute"]}\nPatient info\nEmail: {request.user.username}\nMobile phone: {request.user.phone_number}\nComments: {request.data["comments"]}\n\nKind regards,\nIskender and Murodali'
            send_mail(
                subject, message, EMAIL_HOST_USER, [recepient], fail_silently=False
            )
            models.AppointmentRequest.objects.create(doctor=doctor, description=message)
            return Response("OK")

    def get(self, request):
        if request.user.user_role == 2:
            return Response("You are patient, you view doctor requests.")
        elif request.user.user_role == 1:
            requests = models.AppointmentRequest.objects.filter(doctor=request.user)
            data = serializers.AppointmentRequestSerializer(requests, many=True).data
            return Response(data)

    def delete(self, request):
        try:
            models.AppointmentRequest.objects.get(
                pk=request.query_params.get("pk")
            ).delete()
            return Response("Deleted")
        except:
            return Response("Couldn't find the appointment request with specified pk")


class Contact(APIView):
    def post(self, request):
        message = "\n".join(
            [
                request.data["email"],
                request.data["first_name"],
                request.data["last_name"],
                request.data["message"],
            ]
        )
        send_mail(
            "Contact request",
            message,
            EMAIL_HOST_USER,
            ["iskender.berdiev@gmail.com"],
            fail_silently=False,
        )
        return Response("OK")
