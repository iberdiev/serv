from django.contrib.auth.models import User
from rest_framework import serializers
from .models import (
    CustomUser,
    DoctorSpecialization,
    Profession,
    Doctor,
    AppointmentRequest,
)
from medical_center.models import MedicalCenterDoctor


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = [
            "email",
            "phone_number",
            "birth_date",
            "first_name",
            "last_name",
            "user_role",
        ]


class PatientDetailSerializer(serializers.ModelSerializer):
    location = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = CustomUser
        fields = (
            "email",
            "phone_number",
            "birth_date",
            "location",
            "first_name",
            "last_name",
            "description",
            "profile_image",
        )

    def get_location(self, CustomUser):
        return {
            "city_name": CustomUser.location.city_name,
            "country_name": CustomUser.location.country.country_name,
        }


class DoctorDetailSerializer(serializers.ModelSerializer):
    location = serializers.SerializerMethodField(read_only=True)
    medical_centers = serializers.SerializerMethodField(read_only=True)
    specialization = serializers.SerializerMethodField(read_only=True)
    profession = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = CustomUser
        fields = (
            "pk",
            "email",
            "phone_number",
            "birth_date",
            "location",
            "first_name",
            "last_name",
            "description",
            "medical_centers",
            "specialization",
            "profession",
            "profile_image",
        )

    def get_profession(self, CustomUser):
        ProfessionSerializer
        data = ProfessionSerializer(CustomUser.doctor.profession).data
        return data

    def get_location(self, CustomUser):
        return {
            "city_name": CustomUser.location.city_name,
            "country_name": CustomUser.location.country.country_name,
        }

    def get_medical_centers(self, CustomUser):
        medical_centers = []
        for center in MedicalCenterDoctor.objects.filter(doctor=CustomUser.doctor):
            medical_centers += [{"pk": center.pk, "name": center.medical_center.name}]
        return medical_centers

    def get_specialization(self, CustomUser):
        illnesses = []
        for specialization in DoctorSpecialization.objects.filter(
            doctor__user=CustomUser
        ):
            illnesses += [
                {"pk": specialization.illness.pk, "name": specialization.illness.name}
            ]
        return illnesses


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = (
            "phone_number",
            "location",
            "description",
        )


class ChangePasswordSerializer(serializers.Serializer):
    model = CustomUser
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class UpdateProfileSerializer(serializers.Serializer):
    model = CustomUser
    old_password = serializers.CharField(required=False)
    new_password = serializers.CharField(required=False)
    phone_number = serializers.CharField(required=False)
    birth_date = serializers.DateField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    location = serializers.IntegerField(required=False)
    description = serializers.CharField(required=False)
    profile_image = serializers.ImageField(required=False)


class ProfessionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profession
        fields = (
            "pk",
            "title",
        )


class DoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctor
        fields = ("user", "profession")


class DoctorSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctor
        fields = ("user", "profession")


class AppointmentRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppointmentRequest
        fields = ("description", "pk")