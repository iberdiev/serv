from django.contrib import admin
from .models import (
    CustomUser,
    Patient,
    Doctor,
    DoctorSpecialization,
    Profession,
    AppointmentRequest,
    Example,
)


admin.site.register(CustomUser)
admin.site.register(Patient)
admin.site.register(Doctor)
admin.site.register(DoctorSpecialization)
admin.site.register(Profession)
admin.site.register(AppointmentRequest)
admin.site.register(Example)