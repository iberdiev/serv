# Medical Specialist Search Platform

Django based application that intended to find a medical specialists/doctors and contact them.
Project is especially relevant in remote areas, where local people find difficulties finding specialists.
Currently, project is on development, though the miminum backend functionality and REST endpoints has been developed.
DevOps techniques will be implemented via gitlab-ci and additional bash scripts.

## Usage


```bash
docker-compose build
docker-compose up -d
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.